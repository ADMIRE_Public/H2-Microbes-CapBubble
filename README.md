# H2 Microbes CapBubble

The bubble images and SEM images taken for the study "Microbial induced wettability alteration with implications for Underground Hydrogen Storage" by Maartje Boon, Ivan Buntic, Kadir Ahmed, Nicole Dopffel, Catherine Peters and Hadi Hajibeygi can be found here.
Please contact Dr. Maartje Boon at m.m.boon@tudelft.nl or Dr. Hadi Hajibeygi at h.hajibeygi@tudelft.nl if you have questions about the data.
